# k3s-debian

k3s-debian packages the artifacts from the official k3s releases from their [GitHub](https://github.com/k3s-io/k3s) page into easy installable Debian packages.

# Usage

Add the following configuration to `/etc/apt/sources.list.d/k3s-debian.list`:
```plain
deb https://nopinjector.gitlab.io/k3s-debian unstable main
```

Import the repository signing key from [here](https://nopinjector.gitlab.io/k3s-debian/repo.asc):
```shell
$ gpg repo.asc 
pub   rsa4096 2021-01-31 [SC] [expires: 2031-01-29]
      26F41ACB55B664C6F859524B3AC35592F9CB3858
uid           k3s-debian APT repository signing key

$ sudo apt-key add repo.asc
```

# Exclusion of Liability and Warranty

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
