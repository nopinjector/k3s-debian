#!/bin/sh

. /usr/share/k3s/k3s-shellfunctions
. /usr/share/k3s/k3s-variables

drain_me() {
    local node_name

    node_name=$(get_self_node)
    [ -n "${node_name}" ] || fail 'unable to determine node name'
    kubectl drain "${node_name}" \
        --force \
        --ignore-daemonsets \
        --delete-emptydir-data=true \
        --timeout=15s
}

uncordon_me() {
    local node_name

    node_name=$(get_self_node)
    [ -n "${node_name}" ] || fail 'unable to determine node name'
    kubectl uncordon "${node_name}"
}

cleanup_network() {
    # Remove CNI namespaces
    ip netns show 2>/dev/null | grep cni- | xargs -r -t -n 1 ip netns delete

    # Delete network interface(s) that match 'master cni0'
    ip link show 2>/dev/null | grep 'master cni0' | while read ignore iface ignore; do
        iface=${iface%%@*}
        [ -z "$iface" ] || ip link delete $iface
    done
    ip link delete cni0
    ip link delete flannel.1
    rm -rf /var/lib/cni/
    iptables-save | grep -v KUBE- | grep -v CNI- | iptables-restore
}

cleanup() {
    cleanup_network
}

source_env() {
    if [ -f "${ENV_FILE}" ]; then
        set -a
        . "${ENV_FILE}"
        set +a
    fi
}

start() {
    source_env
    exec "${K3S_BINARY}" "$@" ${CMD_K3S_EXEC}
}

is_configured() {
    source_env
    if [ -n "${K3S_SERVER}" ] && [ "${K3S_SERVER}" -eq 1 ]; then
       exit 0
    fi
    if [ -n "${K3S_URL}" ] && [ -n "${K3S_TOKEN}" ]; then
       exit 0
    fi
    exit 1
}

wait_for() {
    local counter iterations
    iterations="$1"
    shift

    counter=0
    until kubectl version >/dev/null 2>&1; do
        counter=$((counter+1))
        [ $counter -lt $iterations ] || fail "timeout on waiting for k3s"
        sleep 1
    done

    exec "$@"
}

retry() {
    local counter iterations interval
    iterations="$1"
    interval="$2"
    shift; shift

    counter=0
    until $@ >/dev/null 2>&1; do
        counter=$((counter+1))
        [ $counter -lt $iterations ] || fail "retried '$@' $iterations times without success"
        echo "[warning] retrying to execute $@" >&2
        sleep $interval
    done
}

init() {
    local modules

    modules="br_netfilter overlay"
    for module in ${modules}; do
        modprobe ${module}
    done
}

if [ $# -eq 0 ]; then
    cat >&2 <<EOF
usage: $0 <command> [parameters]

possible commands:
    init:           Initialize the runtime for k3s
    start:          Start k3s with sourced default environment.
                    All parameters to this script are passed through to k3s.
    is-configured:  Verifies if k3s is configured
    cleanup:        Cleanup runtime of k3s
    drain-me:       Drain all ressources from this k3s node
    uncordon-me:    Mark this node as schedulable
    wait-for:       Wait until k3s is running, then execute passed commands.
EOF
    exit 1
fi

MODE="$1"
shift

case ${MODE} in
    init)
        init
        ;;
    start)
        start "$@"
        ;;
    is-configured)
        is_configured
        ;;
    cleanup)
        cleanup
        ;;
    drain-me)
        drain_me
        ;;
    uncordon-me)
        uncordon_me
        ;;
    wait-for)
        wait_for "$@"
        ;;
    retry)
        retry "$@"
        ;;
    *)
esac
