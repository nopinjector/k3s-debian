#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
PWD=$(realpath "$(pwd)")

. "${SCRIPT_DIR}/.ci/_functions.sh"

DEFAULT_ARCH=amd64
CACHE_DIR=/tmp/k3s-debian_cache

function mount_bind_repo_in_build() {
    local repo_dir build_dir

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_dir> <build_dir>"
    repo_dir="$1"
    build_dir="$2"

    mkdir -p "${build_dir}/.repo"
    mount --bind "${repo_dir}" "${build_dir}/.repo"
    cat <<< "${build_dir}/.repo"
}

build() {
    local repo_dir arch

    [ $# -le 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_dir> [arch]"
    repo_dir="$1"
    arch="$2"

    (cd "${repo_dir}" && dpkg-buildpackage --host-arch "${arch:-${DEFAULT_ARCH}}" -uc -us -b)
}

build_in_tmp_dir() {
    local repo_dir arch

    [ $# -le 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_dir> [arch]"
    repo_dir="$1"
    arch="$2"

    local tmp_dir=$(mktemp -d)
    trap "rm -rf ${tmp_dir}; trap - RETURN" RETURN INT TERM EXIT

    cp -r "${repo_dir}" "${tmp_dir}/.repo"

    build "${tmp_dir}/.repo" "${arch}"

    mkdir -p "${repo_dir}/_build"
    cp "${tmp_dir}/"*.deb "${repo_dir}/_build"
    cp "${tmp_dir}/"*.changes "${repo_dir}/_build"
}

determine_docker_image() {
    local container_name

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <container-name>"
    container_name="$1"

    if docker image list --format "{{.Repository}}" --filter "reference=${container_name}" | grep -q .; then
        cat - <<< "${container_name}"
    else
        determine_image_from_repository "${container_name}"
    fi
}

determine_podman_image() {
    local container_name

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <container-name>"
    container_name="$1"

    if podman image list --format "{{.Repository}}" --filter "reference=localhost/${container_name}" | grep -q .; then
        cat - <<< "localhost/${container_name}"
    else
        determine_image_from_repository "${container_name}"
    fi
}

determine_image_from_repository() {
    local remote_url repository container_name

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <container-name>"
    container_name="$1"
    repository="origin"

    remote_url=$(git -C "${SCRIPT_DIR}" remote get-url origin)

    if grep -qE '^((https|ssh):\/\/)?(git@)?gitlab.com' <<< "${remote_url}"; then
        sed -e 's/^\(\(https|ssh\):\/\/\)\?\(git@\)\?gitlab.com[/:]/registry.gitlab.com\//' <<< "${remote_url}" |
            sed -e 's/\(\.git\)\?$/\/'${container_name}'/'
    else
        fatal "can not determine container registry from repository url"
    fi
}

build_in_container() {
    local runtime image_ref arch

    [ $# -le 3 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <runtime> <image-ref> [arch]"
    runtime="$1"
    image_ref="$2"
    arch="$3"

    mkdir -p "${SCRIPT_DIR}/_build"

    podman run \
        --rm \
        -v "${SCRIPT_DIR}":/repo:ro \
        -v "${SCRIPT_DIR}/_build":/repo/_build \
        "${image_ref}" \
        /repo/build.sh local /repo/ "${arch}"
}

print_usage_and_die() {
    cat >&2 <<EOF
usage: $0 <ci-runner>

possible ci-runner:
    * unshare: build inside a namespaced (mount, user, pid, ipc) chroot environment
    * local: build on local system into /tmp directory
    * podman: build inside container with podman
    * podman-build: build container with podman
    * docker: build inside container with docker
    * docker-build: build container with docker
    * gitlab: for usage in gitlab ci runners
EOF
    exit 1
}

if [ "${ISOLATION}" == "namespaces" ]; then
    # subsequent stage of unshare isolation
    RUNNER=unshare
else
    [ $# -ge 1 ] || print_usage_and_die
    RUNNER=$1
fi

if [ -n "${CACHE}" ] && [ "${CACHE}" -eq 1 ]; then
    export CACHE_DIR
fi

CONTAINER_IMAGE="k3s-debian-build-env"

case ${RUNNER} in
    unshare)
        isolate_unshare . "$0" "$@"
        mnt_dir=$(mount_bind_repo_in_build /repo-rw /build)
        build "${mnt_dir}" "$2"
        ;;
    local)
        [ $# -eq 2 ] || fatal "usage: $0 local <repo-path>"
        build_in_tmp_dir "$(readlink -e "$2")" "$3"
        ;;
    podman)
        image_ref=$(determine_podman_image "${CONTAINER_IMAGE}")
        build_in_container podman "${image_ref}" "$2"
        ;;
    podman-build)
        (cd "${SCRIPT_DIR}/.ci/container" && podman build --tag "${CONTAINER_IMAGE}" .)
        ;;
    docker)
        image_ref=$(determine_docker_image "${CONTAINER_IMAGE}")
        build_in_container docker "${image_ref}" "$2"
        ;;
    docker-build)
        (cd "${SCRIPT_DIR}/.ci/container" && docker build --tag "${CONTAINER_IMAGE}" .)
        ;;
    gitlab)
        [ -n "${GITLAB_CI}" ] || fatal "This is not a Gitlab CI environment :("
        if [ -n "${CI_DEBUG_TRACE}" ]; then
            set -x
            export DEBUG=1
        fi
        build_in_tmp_dir "${CI_PROJECT_DIR}" "$2"
        ;;
    *)
        print_usage_and_die
        ;;
esac
