#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
. "${SCRIPT_DIR}/_functions.sh"

configure_gnupg() {
    local gnupghome

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <gnupghome>"
    gnupghome="$1"

    cat > "${gnupghome}/gpg.conf" <<EOF
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 BZIP2 ZLIB ZIP Uncompressed
cert-digest-algo SHA512
personal-digest-preferences SHA512 SHA384 SHA256
EOF
}

load_gpg_key() {
    local keyfile passphrase

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <keyfile> <passphrase>"
    keyfile="$1"
    passphrase="$2"

    gpg --batch --yes --no-default-keyring --import \
        <<< $(openssl aes-256-cbc -d -a -in /dev/stdin \
                      -out /dev/stdout -pbkdf2 -k "${passphrase}" <<< "$(cat "${keyfile}" | base64 -d)" \
                      2>/dev/null) >/dev/null 2>&1
    gpg --batch --yes --no-default-keyring --fingerprint --with-colons \
        | grep fpr | head -1 | cut -d ':' -f 10
}

create_dist_config() {
    local conf_template

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <conf_template> <output>"
    template="$1"
    output="$2"

    while read -r line; do
        eval echo "$line"
    done < "${template}" > "${output}"
}

prepare_reprepro() {
    reprepro createsymlinks
}

include_by_changes() {
    local repo_base suite

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_base> <suite>"
    repo_base="$1"
    suite="$2"

    find "${repo_base}" -name '*.changes' -print0 | while read -d $'\0' file
    do
        reprepro include "${suite}" "${file}"
    done
}

export_public_gpg_key() {
    local repo_base suite

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <key-id> <file>"
    key_id="$1"
    output="$2"

    gpg --batch --yes  --no-default-keyring \
        --armor  --output "${output}" --export "${keyid}"
}

sync_result() {
    local repo_base suite

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo-dir> <dest-dir>"
    repo_dir="$1"
    dest_dir="$2"

    mkdir -p "${dest_dir}"
    for p in dists pool repo.asc; do
        cp -r "${repo_dir}/${p}" "${dest_dir}"
    done
}

cleanup() {
    for pid in $(pidof gpg-agent); do
        kill "${pid}"
    done
}

[ $# -eq 3 ] || fatal "usage: $0 <keyfile> <passphrase> <repo-base>"
GPG_KEY="$1"
PASSPHRASE="$2"
REPO_BASE="$3"

tmpdir="$(mktemp --directory --tmpdir pkg-apt.XXXXXXXXXX)"
trap "rm -rf ${chroot_dir}" INT TERM EXIT
mkdir -m 700 -p "${tmpdir}/.gnupg" "${tmpdir}/pkg/conf"

keyid=$(GNUPGHOME="${tmpdir}/.gnupg" load_gpg_key "${GPG_KEY}" "${PASSPHRASE}")
configure_gnupg "${tmpdir}/.gnupg"
create_dist_config "${SCRIPT_DIR}/pkg-repository/distributions" "${tmpdir}/pkg/conf/distributions"
REPREPRO_BASE_DIR="${tmpdir}/pkg/" \
    prepare_reprepro
GNUPGHOME="${tmpdir}/.gnupg" \
REPREPRO_BASE_DIR="${tmpdir}/pkg/" \
    include_by_changes "${REPO_BASE}" unstable
GNUPGHOME="${tmpdir}/.gnupg" export_public_gpg_key "${keyid}" "${tmpdir}/pkg/repo.asc"
cleanup
sync_result "${tmpdir}/pkg" "${SCRIPT_DIR}/../_build/apt-repo"
