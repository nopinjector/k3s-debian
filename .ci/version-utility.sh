#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
. "${SCRIPT_DIR}/_functions.sh"

CHANGELOG_PATH="${SCRIPT_DIR}/../debian/changelog"

K3S_CHANNEL=stable

get_upstream_version() {
    local base_url=https://update.k3s.io/v1-release/channels
    local url="${base_url}/${K3S_CHANNEL}"

    curl -w '%{url_effective}' -L -s -S ${url} -o /dev/null | \
          sed -e 's|.*/||' | tail -c +2
}

get_this_version() {
  local content="$(<"${CHANGELOG_PATH}")"

  head -1 <<< "${content}" | grep -Po '(?<=^k3s \()[^)]+'
}

check_version() {
    local this_version=$(get_this_version)
    local upstream_version=$(get_upstream_version)

    if [ "${this_version}" == "${upstream_version}" ]; then
        echo "already up to date. version=${upstream_version}"
        exit 0
    else
        echo "upstream version ${upstream_version} newer than local version (${this_version})"
        exit 1
    fi
}

update_changelog() {
    [ $# -eq 1 ] || fatal 'update_changelog needs exactly 1 arguments'

    local author="$1"
    local this_version=$(get_this_version)
    local upstream_version=$(get_upstream_version)

    if [ "${this_version}" == "${upstream_version}" ]; then
        return
    fi

    local date=$(date -R)

    local content="$(<"${CHANGELOG_PATH}")"
    cat > "${CHANGELOG_PATH}" <<EOF
k3s (${upstream_version}) unstable; urgency=medium

  * New upstream release ${upstream_version}

 -- ${author}  ${date}

${content}
EOF
}

print_usage_and_die() {
    cat >&2 <<EOF
usage: $0 check-version | update-changelog <author>
EOF
    exit 1
}

[ $# -ge 1 ] || print_usage_and_die

MODE=$1

case ${MODE} in
    check-version)
        check_version
        ;;
    update-changelog)
        [ $# -eq 2 ] || print_usage_and_die
        AUTHOR="$2"
        update_changelog "${AUTHOR}"
        ;;
    *)
        print_usage_and_die
        ;;
esac
