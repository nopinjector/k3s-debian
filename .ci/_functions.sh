# --- verify existence of executable and optionally set path to variable ---
ensure_executable_exist() {
    local exec_name exec_path glob_var

    [ $# -eq 1 ] || [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <executable> [global variable]"
    exec_name="$1"

    # Return failure if it doesn't exist or is no executable
    exec_path="$(which "${exec_name}")"
    [ -x "${exec_path}" ] || return 1

    # Set verified executable as our downloader program and return success
    if [ $# -eq 2 ]; then
        glob_var="$2"
        eval "${glob_var}=\${exec_name}"
    fi

    return 0
}

determine_shell_variant=_internal_determine_shell_variant
_internal_determine_shell_variant() {
  basename "$(readlink -f /proc/$$/exe)"
}

_internal_empty_func() {
  return 0
}

SCRIPT_PATH="$(readlink -f "$0")"
SCRIPT_DIR="$(dirname "${SCRIPT_PATH}")"
SELF_SHELL="$(_internal_determine_shell_variant)"
SELF_SHELL_FULL="$(readlink -f /proc/$$/exe)"

case "${SELF_SHELL}" in
    sh|busybox)
        # Define empty function for local keyword, that's not supported in sh
        local=_internal_empty_func
        ;;
    bash)
        ;;
    dash)
        ;;
    zsh)
        ;;
esac

if [ -n "${DEBUG}" ] && [ "${DEBUG}" -eq 1 ]; then
    SHELL_DEBUG_OPT="-x"
    SHELL_DEBUG_OPT_FULL="${SELF_SHELL_FULL} -x"

    # Apply debug mode also to self
    set -x
fi

# --- download from url ---
HELPER_DOWNLOADER=

download() {
    local dest_file url logfile

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <output> <url>"
    dest_file="$1"
    url="$2"
    _determine_downloader

    logfile="$(mktemp --tmpdir download.XXXXXXXXXX)"
    trap "rm ${logfile}; trap - RETURN" RETURN INT TERM

    case ${HELPER_DOWNLOADER} in
        curl)
            curl -o "${dest_file}" -vfL "${url}" >"${logfile}" 2>&1
            ;;
        wget)
            wget --debug -O "${dest_file}" -o "${logfile}" "${url}" >/dev/null 2>&1
            ;;
    esac

    # Abort if download command failed
    if [ $? -ne 0 ]; then
        cat "${logfile}"
        fatal 'Download failed'
    fi
}

cached_download() {
    local dest_file url url_hash

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <output> <url>"
    dest_file="$1"
    url="$2"

    url_hash=$(echo "${url}" | sha256sum - | cut -d ' ' -f 1)

    if [ ! -z "${CACHE_DIR+x}" ] && [ -f "${CACHE_DIR}/${url_hash}" ]; then
        cp "${CACHE_DIR}/${url_hash}" "${dest_file}"
        return
    fi

    download "${dest_file}" "${url}"

    if [ ! -z "${CACHE_DIR+x}" ]; then
        cp "${dest_file}" "${CACHE_DIR}/${url_hash}"
    fi
}

_determine_downloader() {
    ensure_executable_exist curl HELPER_DOWNLOADER \
        || ensure_executable_exist wget HELPER_DOWNLOADER \
        || fatal 'Can not find curl or wget for downloading files'
}

copy_in_dir() {
  local src dest

  [ $# -eq 2 ] || fatal 'invalid arguments. usage: ${FUNCNAME[0]} <src> <dest>'
  src="$1"
  dest="$2"

  case "${dest}" in
      */) mkdir -p "${dest}"; ;;
  esac
  cp "${src}" "${dest}"
}

# --- helper functions for logs ---
info()
{
    echo '[INFO] ' "$@"
}
warn()
{
    echo '[WARN] ' "$@" >&2
}
fatal()
{
    echo '[ERROR] ' "$@" >&2
    exit 1
}

# --- run the current script in different user namespace ---
isolate_unshare() {
    local repo_root

    [ $# -ge 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_root> [script parameters]"
    repo_root="$(readlink -f "$1")"
    shift

    if [ $(cat /proc/sys/kernel/unprivileged_userns_clone) -eq 0 ]; then
        fatal "please enable unprivileged_userns_clone or run as root! exiting..."
    fi

    if [ -z "${ISOLATION+x}" ]; then
        invoke_in_namespaces "$@"
    elif [ ! -f "/.chroot" ]; then
        pivot_to_chroot "${repo_root}" "$@"
    else
        cd /repo
    fi
}

invoke_in_namespaces() {
    local arg0_path

    arg0_path="$(readlink -f "$0")"
    shift

    # If we create chroot directory here instead of in pivot_to_chroot function
    # umount of bind mounts happen automatically on exit of namespaced process
    # and we can easily rm the chroot directory without explicitly unmounting.
    chroot_dir="$(mktemp --directory --tmpdir unshare.XXXXXXXXXX)"
    trap "rm -rf ${chroot_dir}" INT TERM EXIT

    ISOLATION=namespaces unshare \
        --user \
        --map-root-user \
        --mount \
        --pid \
        --mount-proc \
        --fork \
        --ipc \
        ${SHELL_DEBUG_OPT_FULL} "${arg0_path}" "${chroot_dir}" "$@"
    exit $?
}

pivot_to_chroot() {
    local repo_path chroot_dir self_path chroot_self_path

    [ $# -ge 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <repo_root> [script parameters]"
    repo_root="$1"
    shift

    self_path="$(readlink -f "$0")"
    chroot_self_path="/repo/${self_path##${repo_root}}"
    # Need to shift here: $chroot_self_path replaces $0 in chroot
    shift

    chroot_dir="$1"
    shift

    chroot_env_create "${chroot_dir}" "${repo_root}"

    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
        /usr/sbin/chroot "${chroot_dir}" \
            ${SELF_SHELL_FULL} ${SHELL_DEBUG_OPT} "${chroot_self_path}" "$@"
    exit $?
}

chroot_create_dev() {
    local chroot

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot>"
    chroot="$1"

    mkdir "${chroot}/dev"
    mkdir "${chroot}/dev/pts"
    mkdir "${chroot}/dev/shm"

    chroot_bind_file "${chroot}" /dev/console
    chroot_bind_file "${chroot}" /dev/null
    chroot_bind_file "${chroot}" /dev/ptmx
    chroot_bind_file "${chroot}" /dev/tty
    chroot_bind_file "${chroot}" /dev/urandom
    chroot_bind_file "${chroot}" /dev/zero

    if [ -e /dev/fuse ]; then
        chroot_bind_file "${chroot}" /dev/fuse
    fi

    ln -s /proc/self/fd "${chroot}/dev/fd"
    ln -s /proc/self/fd/0 "${chroot}/dev/stdin"
    ln -s /proc/self/fd/1 "${chroot}/dev/stdout"
    ln -s /proc/self/fd/2 "${chroot}/dev/stderr"
    ln -s /proc/kcore "${chroot}/dev/core"
}

chroot_create_etc() {
    local chroot

    [ $# -eq 1 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot>"
    chroot="$1"

    mkdir "${chroot}/etc"
    mkdir "${chroot}/etc/ssl"

    chroot_copy_host "${chroot}" /etc/group
    chroot_copy_host "${chroot}" /etc/hostname
    chroot_copy_host "${chroot}" /etc/localtime
    chroot_copy_host "${chroot}" /etc/nsswitch.conf
    chroot_copy_host "${chroot}" /etc/passwd
    chroot_copy_host "${chroot}" /etc/resolv.conf
    chroot_copy_host "${chroot}" /etc/login.defs
    chroot_copy_host "${chroot}" /etc/ssl/certs
}

is_overlayfs_available() {
    command -v fuse-overlayfs >/dev/null 2>&1
}

chroot_mount_fuse_overlayfs() {
    local chroot src dest base_dir

    [ $# -eq 3 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <src> <dest>"
    chroot="$1"
    src="$2"
    dest="$3"

    base_dir="${chroot}/tmp/.overlay-$(basename "${src}")-$(echo "${src}${dest}" | sha256sum | cut -b -10)"

    mkdir -p "${base_dir}/upper"
    mkdir -p "${base_dir}/work"
    mkdir "${chroot}${dest}"
    fuse-overlayfs -o "lowerdir=${chroot}${src},upperdir=${base_dir}/upper,workdir=${base_dir}/work" "${chroot}${dest}"
}

chroot_mount_repository() {
    local chroot repo_dir

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <repo_dir>"
    chroot="$1"
    repo_dir="$2"

    chroot_xbind_host_ex rbind "${chroot}" "${repo_dir}" /repo ro

    if [ -d "${repo_dir}/_build" ]; then
        chroot_xbind_host_ex rbind "${chroot}" "${repo_dir}/_build" /build
    fi

    if [ -e /dev/fuse ] && [ is_overlayfs_available ]; then
        chroot_mount_fuse_overlayfs "${chroot}" /repo /repo-rw
    fi
}

chroot_xbind_host_ex() {
    local method chroot path options

    [ $# -ge 3 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <method> <chroot> <target> <mount-point> [options]"
    method="$1"
    chroot="$2"
    target="$3"
    mount_point="$4"
    options="$5"

    if [ "${method}" == "file" ]; then
        touch "${chroot}/${mount_point}"
        method="bind"
    else
        mkdir -p "${chroot}/${mount_point}"
    fi

    if [ -n "${options}" ]; then
        options="-o ${options}"
    fi

    mount "--${method}" ${options} "${target}" "${chroot}${mount_point}"
}

chroot_xbind_host() {
    local method chroot path options

    [ $# -ge 3 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <method> <chroot> <path> [options]"
    method="$1"
    chroot="$2"
    path="$3"
    options="$4"

    chroot_xbind_host_ex "${method}" "${chroot}" "${path}" "${path}" "${options}"
}

chroot_rbind_host() {
    [ $# -ge 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <path> [options]"
    chroot_xbind_host rbind "$@"
}

chroot_bind_host() {
    [ $# -ge 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <path> [options]"
    chroot_xbind_host rbind "$@"
}

chroot_bind_file() {
    [ $# -ge 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <path> [options]"
    chroot_xbind_host file "$@"
}

chroot_symlink_dir() {
    local chroot target link_name

    [ $# -eq 3 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <target> <link_name>"
    chroot="$1"
    target="$2"
    link_name="$3"

    ln -snf "${target}" "${chroot_dir}${link_name}"
}

chroot_tmp_dir() {
    local chroot dir

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <dir>"
    chroot="$1"
    dir="$2"

    mkdir -p ""${chroot}${dir}"";
    mount -t tmpfs tmpfs ""${chroot}${dir}""
}

chroot_copy_host() {
    local chroot path

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <path>"
    chroot="$1"
    path="$2"

    cp -r "${path}" "${chroot}${path}"
}

chroot_env_create() {
    local chroot_dir repo_dir

    [ $# -eq 2 ] || fatal "invalid arguments. usage: ${FUNCNAME[0]} <chroot> <repo_dir>"
    chroot_dir="$1"
    repo_dir="$2"

    chroot_create_dev "${chroot_dir}"
    chroot_create_etc "${chroot_dir}"
    chroot_rbind_host "${chroot_dir}" /usr
    chroot_rbind_host "${chroot_dir}" /var
    chroot_rbind_host "${chroot_dir}" /proc
    chroot_rbind_host "${chroot_dir}" /sys
    chroot_symlink_dir "${chroot_dir}" /usr/lib /lib
    chroot_symlink_dir "${chroot_dir}" /usr/lib64 /lib64
    chroot_symlink_dir "${chroot_dir}" /usr/lib32 /lib32
    chroot_symlink_dir "${chroot_dir}" /usr/bin /bin
    chroot_symlink_dir "${chroot_dir}" /usr/sbin /sbin
    chroot_tmp_dir "${chroot_dir}" /tmp
    chroot_mount_repository "${chroot_dir}" "${repo_dir}"

    if [ ! -z "${CACHE_DIR+x}" ]; then
        chroot_rbind_host  "${chroot_dir}" "${CACHE_DIR}"
    fi

    touch "${chroot_dir}/.chroot"
}