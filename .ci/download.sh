#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
. "${SCRIPT_DIR}/_functions.sh"

GITHUB_URL=https://github.com/k3s-io/k3s/releases

verify_file() {
    [ $# -eq 2 ] || fatal 'download_file needs exactly 3 arguments'
    local file="$1"
    local sha256_file="$2"

    local hash_expected=$(grep " $(basename "${file}")$" "${sha256_file}")
    hash_expected=${hash_expected%%[[:blank:]]*}

    local hash_bin=$(sha256sum "${file}")
    hash_bin=${hash_bin%%[[:blank:]]*}
    if [ "${hash_expected}" != "${hash_bin}" ]; then
        fatal "Download sha256 does not match ${hash_expected}, got ${hash_bin} for file $(basename "${file}")"
    fi
}

download_and_verify() {
    [ $# -eq 3 ] || fatal 'download_and_verify needs exactly 3 arguments'
    local version="$1"
    local arch="$2"
    local dest_dir="$3"
    local suffix

    tmp_dir="$(mktemp -d)"
    trap "rm -rf ${tmp_dir}; trap - RETURN" RETURN INT TERM

    case ${arch} in
      amd64 | x86_64)
          arch=amd64; ;;
      arm64 | aarch64)
          arch=arm64; suffix=-arm64; ;;
      arm*)
          arch=arm; suffix=-armhf; ;;
      *)
          fatal "Unsupported architecture $ARCH"
    esac

    local hash_url="${GITHUB_URL}/download/${version}/sha256sum-${arch}.txt"
    local hash_file="${tmp_dir}/sha256sum.txt"
    download "${hash_file}" "${hash_url}"

    local bin_url="${GITHUB_URL}/download/${version}/k3s${suffix}"
    local bin_file="${tmp_dir}/k3s${suffix}"
    cached_download "${bin_file}" "${bin_url}"

    local images_url="${GITHUB_URL}/download/${version}/k3s-airgap-images-${arch}.tar"
    local images_file="${tmp_dir}/k3s-airgap-images-${arch}.tar"
    cached_download "${images_file}" "${images_url}"

    verify_file "${bin_file}" "${hash_file}"
    verify_file "${images_file}" "${hash_file}"

    copy_in_dir "${bin_file}" "${dest_dir}/k3s"
    copy_in_dir "${images_file}" "${dest_dir}/images/"
}

[ $# -eq 3 ] || fatal 'invalid arguments! usage: $0 <architecture> <version> <dest-dir>'
ARCHITECTURE="$1"
VERSION="$2"
DEST_DIR="$3"

mkdir -p "${DEST_DIR}" || fatal 'can not create destination directory'

download_and_verify "${VERSION}" "${ARCHITECTURE}" "${DEST_DIR}"

